//
//  Item3A.swift
//  ContainerViews

import UIKit

class Item3A: UIViewController {
    
    @IBOutlet weak var But_GoToItem3B: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Not necessary since theprevious view shows both
        //notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":false])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item3A"])
    }
    
    @IBAction func But_GoToItem3B_Tap(_ sender: Any) {
        performSegue(withIdentifier: "Item3BSegue", sender: nil)
    }
}
