//
//  Item6.swift
//  ContainerViews

import UIKit

class Item6: UIViewController {
    
    @IBOutlet weak var But_GoToItem6A: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":false])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item6"])
    }
    
    @IBAction func But_GoToItem6A_Tap(_ sender: Any) {
        performSegue(withIdentifier: "Item6ASegue", sender: nil)
    }
}
