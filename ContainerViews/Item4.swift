//
//  Item4.swift
//  ContainerViews

import UIKit

class Item4: UIViewController {
    
    @IBOutlet weak var But_GoToItem43B: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":false])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item4"])
    }
    
    @IBAction func But_GoToItem43B_Tap(_ sender: Any) {
        performSegue(withIdentifier: "Item43BSegue", sender: nil)
    }
}
