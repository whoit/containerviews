//
//  Item6A.swift
//  ContainerViews

import UIKit

class Item6A: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Not necessary since the previous view shows both
        //notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":false])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item6A"])
    }
}
