//
//  Item2.swift
//  ContainerViews

import UIKit

class Item2: UIViewController {

    @IBOutlet weak var But_GoToItem2A: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":true])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item2"])
    }
    
    @IBAction func But_GoToItem2A_Tap(_ sender: Any) {
        performSegue(withIdentifier: "Item2ASegue", sender: nil)
    }
    
}
