//
//  Menu.swift
//  ContainerViews

import UIKit

class Menu: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":true,"hidebottom":true])
    }
}
