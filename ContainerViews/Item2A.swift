//
//  Item2A.swift
//  ContainerViews

import UIKit

class Item2A: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Not necessary since theprevious view shows both
        //notificationCenter.post(name: NSNotification.Name(rawValue: "hideControlViews"), object: nil, userInfo: ["hidetop":false,"hidebottom":false])
        notificationCenter.post(name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil, userInfo: ["title":"Item2A"])
    }
}
