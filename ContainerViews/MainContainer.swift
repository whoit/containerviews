//
//  MainContainer.swift
//  ContainerViews

import UIKit

let notificationCenter = NotificationCenter.default

class MainContainer: UIViewController {
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var Title_Label: UILabel!
    @IBOutlet weak var MainContainerView: UIView!
    
    var childNavigationController: UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Menu into Container
        let menuVC = storyboard?.instantiateViewController(withIdentifier: "Menu") as? Menu
        childNavigationController.viewControllers = [menuVC] as! [UIViewController]
        
        notificationCenter.addObserver(self, selector: #selector(hideControlViews), name:NSNotification.Name(rawValue: "hideControlViews"), object: nil)
        notificationCenter.addObserver(self, selector: #selector(setTopViewTitle), name:NSNotification.Name(rawValue: "setTopViewTitle"), object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        notificationCenter.removeObserver(self, name: NSNotification.Name(rawValue: "hideControlViews"), object: nil)
        notificationCenter.removeObserver(self, name: NSNotification.Name(rawValue: "setTopViewTitle"), object: nil)
    }
    
    func handleConstraints(){
//        if ((TopView.alpha == 1.0) && (BottomView.alpha == 1.0)) {
//            NSLayoutConstraint.activate([
//                MainContainerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
//                MainContainerView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
//                MainContainerView.topAnchor.constraint(equalTo: self.TopView.bottomAnchor, constant: 0),
//                MainContainerView.bottomAnchor.constraint(equalTo: self.BottomView.topAnchor, constant: 0)
//                ])
//        } else if ((TopView.alpha == 1.0) && (BottomView.alpha == 0.0)) {
//            NSLayoutConstraint.activate([
//                MainContainerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
//                MainContainerView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
//                MainContainerView.topAnchor.constraint(equalTo: self.TopView.bottomAnchor, constant: 0),
//                MainContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
//                ])
//        } else {//((TopView.alpha == 0.0) && (BottomView.alpha == 0.0)) {
            NSLayoutConstraint.activate([
                MainContainerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
                MainContainerView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
                MainContainerView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
                MainContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
                ])
//        }
    }
    
    @IBAction func backButtonTapped() {
        childNavigationController.popViewController(animated: true)
    }
    
    @objc func setTopViewTitle(_ notification: Notification){
        let userInfo = notification.userInfo! as! [String : String]
        //print("Got top title notification: \(String(describing: userInfo["title"]))")
        Title_Label.text = userInfo["title"]
    }
    @objc func hideControlViews(_ notification: Notification){
        let userInfo = notification.userInfo! as! [String : Bool]
        //print("Got top view notification: \(String(describing: userInfo["hide"]))")
        if (userInfo["hidetop"]!){
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {self.TopView.alpha = 0.0})
        }else{
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {self.TopView.alpha = 1.0})
        }
        if (userInfo["hidebottom"]!){
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {self.BottomView.alpha = 0.0})
        }else{
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {self.BottomView.alpha = 1.0})
        }
        handleConstraints()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        childNavigationController = segue.destination as? UINavigationController
    }

}

